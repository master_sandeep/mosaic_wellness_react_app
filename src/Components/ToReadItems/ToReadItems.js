import React, { Component } from 'react';
import ToReadItem from './ToReadItem';
import './toReadItem.css'
import { task } from "../../Store/to_read_items_data";

class ToReadItems extends Component {
  render() {
    return(
      <ul className='ToReadItems'>
        {task.reverse().map((item, index) =>
          <ToReadItem item={item} index={index}></ToReadItem>
        )}
      </ul>);
  }
}

export default ToReadItems;

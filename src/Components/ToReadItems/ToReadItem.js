import React, { Component } from 'react';
import './toReadItem.css'

class ToDoItem extends Component {
  render() {
    const { item, index } = this.props;

    return(
      <li key={index} className='to-read-list-item'>
      <div className='to-read-item'>
        <div className='right-image'>
          <div className='item-name'>
            <img src={item.image} alt={item.name} height="300" width="500"></img>
            <div>
              <p className='name'>{item.name}</p>
              <p className='chevron'>&#x3e;</p>
            </div>
          </div>
          <div className='count'>
            {item.id}
          </div>
        </div>
      </div>
      </li>
    );
  }
}

export default ToDoItem;

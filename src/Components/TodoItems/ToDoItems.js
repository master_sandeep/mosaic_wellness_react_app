import React, { Component } from 'react';
import './toDoItem.css';
import ToDoItem from './ToDoItem';
import { task } from "../../Store/to_do_items_data";

class ToDoItems extends Component {
  render() {
    return(
      <ul>
        {task.map((item, index) =>
          <ToDoItem item={item} index={index}></ToDoItem>
        )}
      </ul>);
  }
}

export default ToDoItems;

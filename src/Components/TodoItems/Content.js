import React, { Component } from 'react';
import './toDoItem.css';

class Content extends Component {
  render() {
    const { item } = this.props;
    return(
      <div className='right-text' key={item.id}>
        <div className='title'>
          <div className='name'>
            {item.name}
          </div>
          <div className='notice'>
            {item.notice}
          </div>
        </div>
        <div className='time'>
          {item.time}
        </div>
      </div>
    );
  }
}

export default Content;

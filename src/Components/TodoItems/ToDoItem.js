import React, { Component } from 'react';
import Content from './Content';
import './toDoItem.css';
import { SwipeableList, SwipeableListItem } from '@sandstreamdev/react-swipeable-list';
import '@sandstreamdev/react-swipeable-list/dist/styles.css';


class ToDoItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null,
      status: null,
      swiped: false
    }
  }

  completed = () => {
    console.log(this.props.item.id + ': completed');
  }
  render() {
    const { item, index } = this.props;

    return (
      <SwipeableList swipeStartThreshold={1}>
        <SwipeableListItem
          swipeLeft={{
            content: <div><button className='completed-btn' onClick={this.completed}>COMPLETED</button></div>,
            action: () => this.setState({ swiped: true })
          }}
          swipeStartThreshold={1}
          blockSwipe={this.state.swiped}
        >
          <li key={index} className='list-item'>
            <div className={`to-do-item ${this.state.swiped ? 'width80' : ''}`}>
              <Content item={item}></Content>
              <div className={'right-button'}>
                <img src={item.icon} className="button-image" height="25px" width="25px" alt='icon' />
              </div>
            </div>
            {this.state.swiped && <div className='right-button-1'><button className='completed-btn' onClick={this.completed}>COMPLETED</button></div>}
          </li>
        </SwipeableListItem>
      </SwipeableList>
    );
  }
}

export default ToDoItem;

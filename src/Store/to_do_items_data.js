import check from '../Assets/check.svg';
import arrow from '../Assets/left-arrow.svg';

export const task = [
    {
      id: 1,
      name: "Hair Appointment",
      notice: "with Dr. Shobita Anand",
      time: "Today, 3:30 PM",
      "icon": check
    },
    {
      id: 2,
      name: "Eat Tablet - Bromide",
      notice: "Follow up on your regime",
      time: "Today, After Lunch",
      icon: arrow
    },
    {
      id: 3,
      name: "Wash Hair",
      notice: "Use your shampoo",
      time: "Today, Morning Bath",
      icon: check
    },
    {
      id: 4,
      name: "Moisturize",
      notice: "Use your moisturizer",
      time: "Before Bedtime, 28th June",
      icon: check
    }
  ]

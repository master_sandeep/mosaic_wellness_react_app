export const task = [
    {
      id: 1,
      name: "EFFECTS OF MINOXIDIL",
      image: 'https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
    },
    {
      id: 2,
      name: "HOW TO USE NIACIN",
      image: 'https://images.unsplash.com/photo-1503803548695-c2a7b4a5b875?ixlib=rb-1.2.1&w=1000&q=80'
    },
    {
      id: 3,
      name: "HAIR CARE 101",
      image: 'https://images.unsplash.com/photo-1535332371349-a5d229f49cb5?ixlib=rb-1.2.1&w=1000&q=80'
    }
  ]

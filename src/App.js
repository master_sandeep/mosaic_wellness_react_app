import React, { Component } from 'react';
import ToDoItems from './Components/TodoItems/ToDoItems';
import ToReadItems from './Components/ToReadItems/ToReadItems';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tab: 'to_do'
    }
  }

  switchTab = (e) => {
    console.log(e.currentTarget.dataset.value)
    this.setState({tab: e.currentTarget.dataset.value});
  }

  render(){
    let component = this.state.tab === 'to_do' ? <ToDoItems /> : <ToReadItems />
    return (
      <div>
        <div className='header'>
          <div className='close'>&#10005;</div>
          <div className='heading'>ACTION ITEMS</div>
        </div>
        <div className='tab'>
          <span className={`to-do ${this.state.tab === 'to_do' ? 'active' : ''}`} onClick={this.switchTab} data-value='to_do'>TO DO</span>
          <span className={`to-read ${this.state.tab === 'to_read' ? 'active' : ''}`} onClick={this.switchTab} data-value='to_read'>TO READ</span>
        </div>
        {component}
      </div>
    );
  }
}

export default App;
